/*!*******************************************************************************************
 *  \file       behavior_rotate_with_pid_control.h
 *  \brief      behavior rotate with pid control  definition file.
 *  \details     This file contains the BehaviorRotateWithPidControl declaration. To obtain more information about
 *              it's definition consult the behavior_rotate_with_pid_control.cpp file.
 *  \authors    Rafael Artiñano Muñoz, Abraham Carrera Groba
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*********************************************************************************************/
#ifndef ROTATE_WITH_PID_CONTROL_H
#define ROTATE_WITH_PID_CONTROL_H

// System
#include <string>
#include <thread>
#include <tuple>
#include <math.h>
#include <yaml-cpp/yaml.h>
#include <Eigen/Geometry>
#include <angles/angles.h>

// ROS
#include "std_srvs/Empty.h"
#include <geometry_msgs/PoseStamped.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>


// Aerostack msgs
#include <aerostack_msgs/BehaviorActivationFinished.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneTrajectoryControllerControlMode.h>
#include <droneMsgsROS/setControlMode.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneDYawCmd.h>
#include<droneMsgsROS/ConsultBelief.h>
#include <droneMsgsROS/droneCommand.h>
#include <droneMsgsROS/askForModule.h>
#include <aerostack_msgs/RequestProcesses.h>


// Aerostack libraries
#include <behavior_execution_controller.h>

namespace quadrotor_motion_with_trajectory_controller
{
class BehaviorRotateWithPidControl : public BehaviorExecutionController
{
  // Constructor
public:
  BehaviorRotateWithPidControl();
  ~BehaviorRotateWithPidControl();

private:

  // Congfig variables
  std::string estimated_pose_str;
  std::string controllers_str;
  std::string yaw_controller_str;
  std::string service_topic_str;
  std::string drone_position_str;
  std::string yaw_to_look_str;
  std::string drone_yaw_ref_str;
  std::string drone_control_mode_str;
  std::string execute_query_srv;
  std::vector<std::string> processes;
  double target_yaw;

  ros::NodeHandle nh;
  std::string nspace;

  // Communication variables
  ros::Subscriber estimated_pose_sub;
  ros::Publisher controllers_pub;
  ros::Publisher drone_position_pub;
  ros::Publisher  yaw_command_pub;
  ros::ServiceClient mode_service;
  ros::ServiceClient query_client;
  ros::ServiceClient request_processes_activation_cli;
  ros::ServiceClient request_processes_deactivation_cli;

  // Messages
  geometry_msgs::PoseStamped estimated_pose_msg;


private:
  // BehaviorExecutionController
  void onConfigure();
  void onActivate();
  void onDeactivate();
  void onExecute();
  bool checkSituation();
  void checkGoal();
  void checkProgress();
  void checkProcesses();

private:
  void toEulerAngle(const Eigen::Quaternionf& q, float& roll, float& pitch, float& yaw);

public: // Callbacks
  void estimatedPoseCallBack(const geometry_msgs::PoseStamped &);


};
}

#endif
