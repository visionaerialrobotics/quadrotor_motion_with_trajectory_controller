/*!*******************************************************************************************
 *  \file       behavior_keep_moving_with_pid_control.h
 *  \brief      behavior keep moving with pid control definition file.
 *  \details     This file contains the behaviorKeepMoving declaration. To obtain more information about
 *              it's definition consult the behavior_keep_moving_with_pid_control.cpp file.
 *  \authors    Rafael Artiñano Muñoz, Abraham Carrera Groba
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef KEEP_MOVING_WITH_PID_CONTROL_H
#define KEEP_MOVING_WITH_PID_CONTROL_H

// System
#include <string>
#include <thread>
#include <tuple>
// ROS
#include "std_srvs/Empty.h"
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <yaml-cpp/yaml.h>

// Aerostack msgs
#include <aerostack_msgs/BehaviorActivationFinished.h>
#include <aerostack_msgs/RequestProcesses.h>
#include <droneMsgsROS/ConsultBelief.h>
#include <droneMsgsROS/droneCommand.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneTrajectoryControllerControlMode.h>
#include <droneMsgsROS/setControlMode.h>
#include <droneMsgsROS/dronePitchRollCmd.h>
#include <droneMsgsROS/droneDAltitudeCmd.h>
#include <droneMsgsROS/droneDYawCmd.h>
#include <droneMsgsROS/droneSpeeds.h>




// Aerostack libraries
#include <behavior_execution_controller.h>

namespace quadrotor_motion_with_trajectory_controller
{
class BehaviorKeepMovingWithPidControl : public BehaviorExecutionController
{
  // Constructor
public:
  BehaviorKeepMovingWithPidControl();
  ~BehaviorKeepMovingWithPidControl();

private:

  // Congfig variables
  std::string estimated_pose_str;
  std::string rotation_angles_str;
  std::string controllers_str;
  std::string estimated_speed_str;
  std::string yaw_controller_str;
  std::string service_topic_str;
  std::string drone_position_str;
  std::string speed_topic;
  std::string drone_control_mode_str;
  std::string d_altitude_str;
  std::string execute_query_srv;
  std::vector<std::string> processes;
  // Communication variables
  ros::Subscriber estimated_pose_sub;
  ros::Subscriber estimated_speed_sub;
  ros::Subscriber rotation_angles_sub;
  ros::Publisher controllers_pub;
  ros::Publisher  speed_topic_pub;
  ros::ServiceClient mode_service;
  ros::ServiceClient query_client;
  ros::ServiceClient request_processes_activation_cli;
  ros::ServiceClient request_processes_deactivation_cli;
  ros::NodeHandle nh;
  std::string nspace;
  // Messages
  geometry_msgs::PoseStamped estimated_pose_msg;
  geometry_msgs::TwistStamped target_position;
  geometry_msgs::TwistStamped estimated_speed_msg;
  geometry_msgs::Vector3Stamped rotation_angles_msg;


private:
  // BehaviorExecutionController
  void onConfigure();
  void onActivate();
  void onDeactivate();
  void onExecute();
  bool checkSituation();
  void checkGoal();
  void checkProgress();
  void checkProcesses();


public: // Callbacks
  void estimatedPoseCallBack(const geometry_msgs::PoseStamped&);
  void estimatedSpeedCallback(const geometry_msgs::TwistStamped&);
  void rotationAnglesCallback(const geometry_msgs::Vector3Stamped&);

};
}

#endif
