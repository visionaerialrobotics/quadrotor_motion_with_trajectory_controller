/*!*******************************************************************************************
 *  \file       behavior_rotate_with_pid_control.cpp
 *  \brief      Behavior Rotate with PID Control implementation file.
 *  \details    This file implements the BehaviorRotateWithPidControl class.
 *  \authors    Rafael Artiñano Muñoz, Abraham Carrera Groba
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/behavior_rotate_with_pid_control.h"
#include <pluginlib/class_list_macros.h>

namespace quadrotor_motion_with_trajectory_controller
{
BehaviorRotateWithPidControl::BehaviorRotateWithPidControl() : BehaviorExecutionController() 
{ 
  setName("rotate_with_pid_control");
}

BehaviorRotateWithPidControl::~BehaviorRotateWithPidControl() {}

bool BehaviorRotateWithPidControl::checkSituation()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    setErrorMessage("Error: Battery low, unable to perform action");
    return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
   setErrorMessage("Error: Drone landed");
   return false;
  }

  return true;
}



void BehaviorRotateWithPidControl::checkGoal() {

   float angle_variation_maximum=0.1;

   Eigen::Quaternionf q(
      estimated_pose_msg.pose.orientation.w, estimated_pose_msg.pose.orientation.x,
      estimated_pose_msg.pose.orientation.y, estimated_pose_msg.pose.orientation.z);
      
    float yaw, pitch, roll;
    toEulerAngle(q, roll, pitch, yaw);

   if (yaw < 0)
   {
     yaw=2*M_PI + yaw;
   }
   if(std::abs(target_yaw-yaw)<angle_variation_maximum)
   {
     BehaviorExecutionController::setTerminationCause(aerostack_msgs::BehaviorActivationFinished::GOAL_ACHIEVED);
   }
 
}

void BehaviorRotateWithPidControl::checkProgress() 
{ 

}

void BehaviorRotateWithPidControl::checkProcesses() 
{ 

}

void BehaviorRotateWithPidControl::onConfigure()
{
  nh = getNodeHandle();
  nspace = getNamespace();
  nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "self_localization/pose");
  nh.param<std::string>("controllers_topic", controllers_str, "command/high_level");
  nh.param<std::string>("yaw_controller_str",yaw_controller_str , "droneControllerYawRefCommand");
  nh.param<std::string>("service_topic_str",service_topic_str , "droneTrajectoryController/setControlMode");
  nh.param<std::string>("drone_position_str",drone_position_str , "dronePositionRefs");
  nh.param<std::string>("drone_yaw_to_look_str",yaw_to_look_str, "droneYawToLook");
  nh.param<std::string>("drone_yaw_ref",drone_yaw_ref_str,"droneControllerYawRefCommand");
  nh.param<std::string>("drone_control_mode",drone_control_mode_str,"droneTrajectoryController/controlMode");
  nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
 
  query_client = nh.serviceClient <droneMsgsROS::ConsultBelief> ("/" + nspace + "/" +execute_query_srv);

}

void BehaviorRotateWithPidControl::onActivate()
{ 
   // Start processes
  ros::ServiceClient start_controller=nh.serviceClient<std_srvs::Empty>("/"+nspace+"/droneTrajectoryController/start");
  std_srvs::Empty req;
  start_controller.call(req);

  // Activate communications

  yaw_command_pub=nh.advertise<droneMsgsROS::droneYawRefCommand>("/" + nspace + "/" +drone_yaw_ref_str,10,true);
  mode_service=nh.serviceClient<droneMsgsROS::setControlMode>("/" + nspace + "/" +service_topic_str);
  controllers_pub = nh.advertise<droneMsgsROS::droneCommand>("/" + nspace + "/" +controllers_str, 10, true);

  estimated_pose_sub = nh.subscribe("/" + nspace + "/" +estimated_pose_str, 1000, &BehaviorRotateWithPidControl::estimatedPoseCallBack, this);

  drone_position_pub=nh.advertise< droneMsgsROS::dronePositionRefCommandStamped>("/" + nspace + "/" +drone_position_str,10,true);

  estimated_pose_msg = *ros::topic::waitForMessage<geometry_msgs::PoseStamped>("/" + nspace + "/" +estimated_pose_str, nh, ros::Duration(2));

  // Behavior implementation
  std::string arguments=getParameters();
  YAML::Node config_file = YAML::Load(arguments);
  if(config_file["angle"].IsDefined())
  {
    double angle=config_file["angle"].as<double>() * M_PI/180;
    target_yaw=angle;
  }
  else
  {
    if(config_file["relative_angle"].IsDefined())
    {
      double angle=config_file["relative_angle"].as<double>() * M_PI/180;
      Eigen::Quaternionf q(
      estimated_pose_msg.pose.orientation.w, estimated_pose_msg.pose.orientation.x,
      estimated_pose_msg.pose.orientation.y, estimated_pose_msg.pose.orientation.z);
      
      float yaw, pitch, roll;

      toEulerAngle(q, roll, pitch, yaw);

      target_yaw= yaw + angle;
      if(target_yaw> 2* M_PI || target_yaw < -2*M_PI)
      {
       target_yaw=target_yaw*180/M_PI;
       target_yaw= fmod(target_yaw,360);
       target_yaw=target_yaw*M_PI/180;
      }

   }
   else{
      setErrorMessage("Angle not defined");
      return;
   }
 }

  droneMsgsROS::setControlMode mode;
  mode.request.controlMode.command=mode.request.controlMode.POSITION_CONTROL;
  mode_service.call(mode);

  // Send reference to controller
  droneMsgsROS::dronePositionRefCommandStamped reference_position_msg;
  reference_position_msg.position_command.x = estimated_pose_msg.pose.position.x;
  reference_position_msg.position_command.y = estimated_pose_msg.pose.position.y;
  reference_position_msg.position_command.z = estimated_pose_msg.pose.position.z;
  reference_position_msg.header.stamp = ros::Time::now();

  drone_position_pub.publish(reference_position_msg);

  // Wait for controller to enter POSITION mode
  ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
   "/" + nspace + "/" + drone_control_mode_str, nh
  );

  // Send target yaw
  droneMsgsROS::droneYawRefCommand yaw_msg;
  yaw_msg.yaw=target_yaw;
  yaw_command_pub.publish(yaw_msg);

  if (target_yaw<0)
  {
    target_yaw=2*M_PI + target_yaw;
  }
  droneMsgsROS::droneCommand msg;
  msg.command = droneMsgsROS::droneCommand::MOVE;
  controllers_pub.publish(msg);
}

void BehaviorRotateWithPidControl::onDeactivate()
{
  ros::ServiceClient stop_controller=nh.serviceClient<std_srvs::Empty>("/"+nspace+"/droneTrajectoryController/stop");
  std_srvs::Empty req;
  stop_controller.call(req);
  
  droneMsgsROS::droneCommand msg;
  msg.command = droneMsgsROS::droneCommand::HOVER;
  controllers_pub.publish(msg);
  estimated_pose_sub.shutdown();
  
  yaw_command_pub.shutdown();
  mode_service.shutdown();
  controllers_pub.shutdown();
  drone_position_pub.shutdown();
}

void BehaviorRotateWithPidControl::onExecute()
{
  
}
void BehaviorRotateWithPidControl::toEulerAngle(const Eigen::Quaternionf& q, float& roll, float& pitch, float& yaw)
{
  // roll (x-axis rotation)
  double sinr_cosp = +2.0 * (q.w() * q.x() + q.y() * q.z());
  double cosr_cosp = +1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y());
  roll = atan2(sinr_cosp, cosr_cosp);

  // pitch (y-axis rotation)
  double sinp = +2.0 * (q.w() * q.y() - q.z() * q.x());
  if (fabs(sinp) >= 1)
    pitch = copysign(M_PI / 2, sinp);  // use 90 degrees if out of range
  else
    pitch = asin(sinp);

  // yaw (z-axis rotation)
  double siny_cosp = +2.0 * (q.w() * q.z() + q.x() * q.y());
  double cosy_cosp = +1.0 - 2.0 * (q.y() * q.y() + q.z() * q.z());
  yaw = atan2(siny_cosp, cosy_cosp);
}

void BehaviorRotateWithPidControl::estimatedPoseCallBack(const geometry_msgs::PoseStamped& msg)
{
    estimated_pose_msg=msg;  
}

}
PLUGINLIB_EXPORT_CLASS(quadrotor_motion_with_trajectory_controller::BehaviorRotateWithPidControl, nodelet::Nodelet)
