/*!*******************************************************************************************
 *  \file       behavior_keep_hovering_with_pid_control.cpp
 *  \brief      Behavior BehaviorKeepHoveringWithPidControl implementation file.
 *  \details    This file implements the BehaviorKeepHoveringWithPidControl class.
 *  \authors    Rafael Artiñano Muñoz, Abraham Carrera Groba
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/behavior_keep_hovering_with_pid_control.h"
#include <pluginlib/class_list_macros.h>

namespace quadrotor_motion_with_trajectory_controller
{
BehaviorKeepHoveringWithPidControl::BehaviorKeepHoveringWithPidControl() : BehaviorExecutionController() { 
setName("keep_hovering_with_pid_control");
setExecutionGoal(ExecutionGoals::KEEP_RUNNING);
}

BehaviorKeepHoveringWithPidControl::~BehaviorKeepHoveringWithPidControl() {}

bool BehaviorKeepHoveringWithPidControl::checkSituation()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    setErrorMessage("Error: Battery low, unable to perform action");
    return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,FLYING)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(!query_service.response.success)
  { 
    setErrorMessage("Error: Drone landed");
    return false;
  }


  return true;
}


void BehaviorKeepHoveringWithPidControl::checkGoal() 
{ 

}

void BehaviorKeepHoveringWithPidControl::checkProgress()
{ 

}

void BehaviorKeepHoveringWithPidControl::checkProcesses()
{ 

}

void BehaviorKeepHoveringWithPidControl::onConfigure()
{
  ros::NodeHandle nh = getNodeHandle();
  std::string nspace = getNamespace();
  nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "self_localization/pose");
  nh.param<std::string>("controllers_topic", controllers_str, "command/high_level");
  nh.param<std::string>("speed_reset", speed_reset_str,"droneSpeedsRefs");
  nh.param<std::string>("drone_pitch_roll",drone_pitch_roll_str,"command/pitch_roll");
  nh.param<std::string>("d_yaw",d_yaw_str,"command/dYaw");
  nh.param<std::string>("d_altitude",d_altitude_str,"command/dAltitude");
  nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
 
  query_client = nh.serviceClient <droneMsgsROS::ConsultBelief> ("/" + nspace + "/" +execute_query_srv);
}

void BehaviorKeepHoveringWithPidControl::onActivate()
{

  // Activate communications
  ros::NodeHandle nh = getNodeHandle();
  std::string nspace = getNamespace();
  controllers_pub = nh.advertise<droneMsgsROS::droneCommand>("/" + nspace + "/" +controllers_str, 1000, true);
  estimated_pose_sub = nh.subscribe("/" + nspace + "/" +estimated_pose_str, 1000, &BehaviorKeepHoveringWithPidControl::estimatedPoseCallBack, this);
  drone_speeds_reseter_pub = nh.advertise<droneMsgsROS::droneSpeeds>("/" + nspace + "/" +speed_reset_str, 10,true);
  d_pitch_roll_pub = nh.advertise<droneMsgsROS::dronePitchRollCmd>("/" + nspace + "/" +drone_pitch_roll_str,10,true);
  d_yaw_pub = nh.advertise<droneMsgsROS::droneDYawCmd>("/" + nspace + "/" +d_yaw_str,10,true);
  d_altitude_pub = nh.advertise<droneMsgsROS::droneDAltitudeCmd>("/" + nspace + "/" +d_altitude_str,10,true);


  // Behavior implementation
  droneMsgsROS::dronePitchRollCmd pitch_roll_msg;
  droneMsgsROS::droneDAltitudeCmd d_altitude_msg;
  droneMsgsROS::droneDYawCmd d_yaw_msg;
  droneMsgsROS::droneSpeeds reset;
  reset.dx=0.0;
  reset.dy=0.0;
  reset.dz=0.0;
  reset.dyaw=0.0;
  reset.dpitch=0.0;
  reset.droll=0.0;
  pitch_roll_msg.pitchCmd = 0.0;
  pitch_roll_msg.rollCmd = 0.0;
  d_altitude_msg.dAltitudeCmd = 0.0;
  d_yaw_msg.dYawCmd = 0.0;

  d_altitude_pub.publish(d_altitude_msg);
  d_yaw_pub.publish(d_yaw_msg);
  d_pitch_roll_pub.publish(pitch_roll_msg);
  drone_speeds_reseter_pub.publish(reset);
  std_msgs::Header header;
  header.frame_id = "behavior_keep_hovering_with_pid_control";
  droneMsgsROS::droneCommand msg;
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::HOVER;
  controllers_pub.publish(msg);

  estimated_pose_msg = *ros::topic::waitForMessage<geometry_msgs::PoseStamped>("/" + nspace + "/" +estimated_pose_str, nh);
  static_pose.pose.position.x=estimated_pose_msg.pose.position.x;
  static_pose.pose.position.y=estimated_pose_msg.pose.position.y;
  static_pose.pose.position.z=estimated_pose_msg.pose.position.z;
}

void BehaviorKeepHoveringWithPidControl::onDeactivate()
{
  controllers_pub.shutdown();
  estimated_pose_sub.shutdown();
  drone_speeds_reseter_pub.shutdown();
  d_pitch_roll_pub.shutdown();
  d_yaw_pub.shutdown();
  d_altitude_pub.shutdown();
}

void BehaviorKeepHoveringWithPidControl::onExecute()
{
  
}

// Custom topic Callbacks
void BehaviorKeepHoveringWithPidControl::estimatedPoseCallBack(const geometry_msgs::PoseStamped &message)
{
    estimated_pose_msg = message;
}


}
PLUGINLIB_EXPORT_CLASS(quadrotor_motion_with_trajectory_controller::BehaviorKeepHoveringWithPidControl, nodelet::Nodelet)
