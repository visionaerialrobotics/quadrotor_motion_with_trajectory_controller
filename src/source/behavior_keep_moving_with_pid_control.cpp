/*!*******************************************************************************************
 *  \file       behavior_keep_moving_with_pid_control.cpp
 *  \brief      Behavior Keep Moving with PID Control implementation file.
 *  \details    This file implements the BehaviorKeepMovingWithPidControl class.
 *  \authors    Rafael Artiñano Muñoz, Abraham Carrera Groba
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/behavior_keep_moving_with_pid_control.h"
#include <pluginlib/class_list_macros.h>

namespace quadrotor_motion_with_trajectory_controller
{
BehaviorKeepMovingWithPidControl::BehaviorKeepMovingWithPidControl() : BehaviorExecutionController() { 
setName("keep_moving_with_pid_control");
setExecutionGoal(ExecutionGoals::KEEP_RUNNING);
}

BehaviorKeepMovingWithPidControl::~BehaviorKeepMovingWithPidControl() {}

bool BehaviorKeepMovingWithPidControl::checkSituation()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    setErrorMessage("Error: Battery low, unable to perform action");
    return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    setErrorMessage("Error: Drone landed");
    return false;
  }

  return true;
}



void BehaviorKeepMovingWithPidControl::checkGoal() 
{ 

}

void BehaviorKeepMovingWithPidControl::checkProgress() 
{
 
}

void BehaviorKeepMovingWithPidControl::checkProcesses() 
{ 
 
}

void BehaviorKeepMovingWithPidControl::onConfigure()
{
    nh = getNodeHandle();
    nspace = getNamespace();
    nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "self_localization/pose");
    nh.param<std::string>("controllers_topic", controllers_str, "command/high_level");
    nh.param<std::string>("rotation_angles_topic", rotation_angles_str, "rotation_angles");
    nh.param<std::string>("estimated_speed_topic",estimated_speed_str,"self_localization/speed");
    nh.param<std::string>("yaw_controller_str",yaw_controller_str , "droneControllerYawRefCommand");
    nh.param<std::string>("service_topic_str",service_topic_str , "droneTrajectoryController/setControlMode");
    nh.param<std::string>("drone_position_str",drone_position_str , "dronePositionRefs");
    nh.param<std::string>("speed_topic",speed_topic , "droneSpeedsRefs");
    nh.param<std::string>("drone_control_mode",drone_control_mode_str,"droneTrajectoryController/controlMode");
    nh.param<std::string>("d_altitude",d_altitude_str,"command/dAltitude");
    nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
 
  query_client = nh.serviceClient <droneMsgsROS::ConsultBelief> ("/" + nspace + "/" +execute_query_srv);
}

void BehaviorKeepMovingWithPidControl::onActivate()
{ 
   // Start processes
  ros::ServiceClient start_controller=nh.serviceClient<std_srvs::Empty>("/"+nspace+"/droneTrajectoryController/start");
  std_srvs::Empty req;
  start_controller.call(req);

  // Activate communications
    estimated_pose_sub = nh.subscribe("/" + nspace + "/" +estimated_pose_str, 1000, &BehaviorKeepMovingWithPidControl::estimatedPoseCallBack, this);
    rotation_angles_sub = nh.subscribe("/" + nspace + "/" +rotation_angles_str, 1000, &BehaviorKeepMovingWithPidControl::rotationAnglesCallback, this);
    estimated_speed_sub = nh.subscribe("/" + nspace + "/" +estimated_speed_str, 1000, &BehaviorKeepMovingWithPidControl::estimatedSpeedCallback, this);
    controllers_pub = nh.advertise<droneMsgsROS::droneCommand>("/" + nspace + "/" +controllers_str, 10, true);
    mode_service=nh.serviceClient<droneMsgsROS::setControlMode>("/" + nspace + "/" +service_topic_str);
    speed_topic_pub=nh.advertise<droneMsgsROS::droneSpeeds>("/" + nspace + "/" +speed_topic,10, true);


  // Behavior implementation
    std::string arguments=getParameters();
    YAML::Node config_file = YAML::Load(arguments);

    std::string direction = config_file["direction"].as<std::string>();
    double speed = config_file["speed"].as<double>();
    if(direction == "UP")
    {
      target_position.twist.linear.x=0;
      target_position.twist.linear.y=0;
      target_position.twist.linear.z=speed;

    }
    else if(direction == "DOWN")
    {
      target_position.twist.linear.x=0;
      target_position.twist.linear.y=0;
      target_position.twist.linear.z=-speed;

    }
    else if(direction == "LEFT")
    {
      target_position.twist.linear.x=-speed;
      target_position.twist.linear.y=0;
      target_position.twist.linear.z=0;

    }
    else if(direction == "RIGHT")
    {
      target_position.twist.linear.x=speed;
      target_position.twist.linear.y=0;
      target_position.twist.linear.z=0;

    }
    else if(direction == "FORWARD")
    {
      target_position.twist.linear.x=0;
      target_position.twist.linear.y=speed;
      target_position.twist.linear.z=0;
    }
    else if(direction == "BACKWARD")
    {
      target_position.twist.linear.x=0;
      target_position.twist.linear.y=-speed;
      target_position.twist.linear.z=0;

    }
    else
    {
      std::cout<<"argument given is invalid"<< std::endl;
      return;
    }

    estimated_speed_msg = *ros::topic::waitForMessage<geometry_msgs::TwistStamped>("/" + nspace + "/" +estimated_speed_str, nh, ros::Duration(2));
    estimated_pose_msg = *ros::topic::waitForMessage<geometry_msgs::PoseStamped>("/" + nspace + "/" +estimated_pose_str, nh, ros::Duration(2));

    droneMsgsROS::setControlMode mode;
    mode.request.controlMode.command=mode.request.controlMode.SPEED_CONTROL;
    mode_service.call(mode);

    droneMsgsROS::droneSpeeds point;
    point.dx=estimated_speed_msg.twist.linear.x;
    point.dy=estimated_speed_msg.twist.linear.y;
    point.dz=estimated_speed_msg.twist.linear.z;
    speed_topic_pub.publish(point);

    ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
      "/" + nspace + "/" +drone_control_mode_str, nh
    );
      point.dx=target_position.twist.linear.x;
      point.dy=target_position.twist.linear.y;
      point.dz=target_position.twist.linear.z;
      speed_topic_pub.publish(point);

      droneMsgsROS::droneCommand msg;
      msg.command = droneMsgsROS::droneCommand::MOVE;
      controllers_pub.publish(msg);

      estimated_speed_msg = *ros::topic::waitForMessage<geometry_msgs::TwistStamped>("/" + nspace + "/" +estimated_speed_str, nh, ros::Duration(2));

}

void BehaviorKeepMovingWithPidControl::onDeactivate()
{ 
  ros::ServiceClient stop_controller=nh.serviceClient<std_srvs::Empty>("/"+nspace+"/droneTrajectoryController/stop");
  std_srvs::Empty req;
  stop_controller.call(req);

  estimated_pose_sub.shutdown();
  rotation_angles_sub.shutdown();
  estimated_speed_sub.shutdown();
  controllers_pub.shutdown();
  mode_service.shutdown();
  speed_topic_pub.shutdown();
}

void BehaviorKeepMovingWithPidControl::onExecute()
{
  
}


void BehaviorKeepMovingWithPidControl::estimatedSpeedCallback(const geometry_msgs::TwistStamped& msg)
{
    estimated_speed_msg=msg;

}
void BehaviorKeepMovingWithPidControl::estimatedPoseCallBack(const geometry_msgs::PoseStamped& msg)
{
    estimated_pose_msg=msg;
}
void BehaviorKeepMovingWithPidControl::rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
    rotation_angles_msg=msg;
}

}
PLUGINLIB_EXPORT_CLASS(quadrotor_motion_with_trajectory_controller::BehaviorKeepMovingWithPidControl, nodelet::Nodelet)
